# Создать программу, которая посчитает средний рост и вес студентов из
# файла hw.csv и средне квадратическое отклонение.
import csv
import math


def average_values():
    """formula for calculating the average height and weight and their root-mean-square number"""
    with open('hw.csw') as file:
        reader = csv.DictReader(file, delimiter=',')
        general_height = 0
        general_weight = 0
        heights_list = []
        weight_list = []
        for row in reader:
            general_height += float(row[' "Height(Inches)"'])
            heights_list.append(float(row[' "Height(Inches)"']))
            general_weight += float(row[' "Weight(Pounds)"'])
            weight_list.append(float(row[' "Weight(Pounds)"']))
        average_height = (general_height / int(row["Index"]))
        average_weight = (general_weight / int(row["Index"]))
        sum_of_squares_deviations_heights = 0
        for i in heights_list:
            sum_of_squares_deviations_heights += (average_height - i) ** 2
        root_mean_square_height = math.sqrt(sum_of_squares_deviations_heights / (int(row["Index"]) - 1))
        sum_of_squares_deviations_weight = 0
        for v in weight_list:
            sum_of_squares_deviations_weight += (average_weight - v) ** 2
        root_mean_square_weights = math.sqrt(sum_of_squares_deviations_weight / (int(row["Index"]) - 1))

        return f'Average height - {average_height},root mean square height- {root_mean_square_height}' \
               f' , average weight - {average_weight}, root mean square weight - {root_mean_square_weights} '


print(average_values())
